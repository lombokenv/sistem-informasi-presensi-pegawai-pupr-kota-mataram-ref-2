<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PresensiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
 
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id_presensi' => $this->id_presensi,
            'id_pegawai'=>$this->id_pegawai,
            'tanggal' => $this->tanggal,
            'absen_pagi' => $this->absen_pagi,
            'absen_sore' => $this->absen_sore,
            'pegawai' => $this->name,
            'status_kepegawaian'=> $this->status_kepegawaian,
        
        ];
    }
}
