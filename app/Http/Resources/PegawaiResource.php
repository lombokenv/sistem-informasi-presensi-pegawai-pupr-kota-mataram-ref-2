<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PegawaiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'alamat'=>$this->alamat,
            'nip'=>$this->nip,
            'bidang'=>$this->bidang,
            'status_kepegawaian'=>$this->status_kepegawaian,
            'jenis_kelamin'=>$this->jenis_kelamin
        ];
    }
}
