<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Http\Resources\UserResource;
class UserController extends Controller
{
    public function __construct(){
        $this->middleware('JWT', ['except' => ['edit']]);
    }

    public function index()
    {
        return UserResource::collection(User::latest()->get());
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        return response()->json($user,Response::HTTP_CREATED);
        // return response('created', Response::HTTP_CREATED);
    }

    
    public function show(User $user)
    {
        return new UserResource($user);
    }

   
    public function edit(User $user)
    {
        //
    }

    public function update(Request $request, User $user)
    {
            $user->update($request->all());
            return response('update', Response::HTTP_ACCEPTED);
    }

   
    public function destroy(User $user)
    {
        $user->delete();
        return response('delete',Response::HTTP_NO_CONTENT);
    }
}
