<?php

namespace App\Http\Controllers;

use App\Model\Pegawai;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Http\Resources\PegawaiResource;

class PegawaiController extends Controller
{   
    public function __construct(){
        $this->middleware('JWT', ['except' => ['index','show']]);
    }
    
    public function index()
    {
        return PegawaiResource::collection(Pegawai::latest()->get());
             
    }

    public function store(Request $request){
        $pegawai = Pegawai::create($request->all());
        return response()->json($pegawai,Response::HTTP_CREATED);
        // return response('created', Response::HTTP_CREATED);
    }
   
    public function show(String $pegawai)
    {
        // return new PegawaiResource($pegawai);
        $result = Pegawai::where('id', $pegawai)->get();
        return PegawaiResource::collection($result);
    }

    public function showStatus(String $pegawai)
    {
        // return new PegawaiResource($pegawai);
        $result = Pegawai::where('status_kepegawaian', $pegawai)->get();
        return PegawaiResource::collection($result);
    }
   
    public function update(Request $request, Pegawai $pegawai){
        $pegawai->update($request->all());
        return response('update', Response::HTTP_ACCEPTED);
    }
    public function destroy(Pegawai $pegawai){
        $pegawai->delete();
        return response('deleted',Response::HTTP_NO_CONTENT);
    }

    
}
