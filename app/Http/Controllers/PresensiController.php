<?php

namespace App\Http\Controllers;

use App\Model\Presensi;
use App\Http\Resources\PresensiResource;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;

class PresensiController extends Controller
{
    public function __construct(){
        $this->middleware('JWT', ['except' => ['index','show']]);
    }
    
    public function index()
    {
        
        $result = Presensi::with('pegawai')
        ->join('pegawais','presensis.id_pegawai','=','pegawais.id')
        ->get();
        return PresensiResource::collection($result);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $presensi = Presensi::create($request->all());
        return response()->json($presensi,Response::HTTP_CREATED);
        // return response('created', Response::HTTP_CREATED);
    }

   
    public function show(int $presensi)
    {

        $result = Presensi::where('id_presensi', $presensi)
        ->join('pegawais','presensis.id_pegawai','=','pegawais.id')
        ->get();
         return PresensiResource::collection($result);
    } 


    public function update(Request $request, int $presensi)
    {
        Presensi::where('id_presensi',$presensi)->update($request->all());
        return response('update', Response::HTTP_ACCEPTED);
    }


    public function destroy(int $presensi)
    {
        // $presensi->delete();
        Presensi::where('id_presensi', $presensi)->delete();
        return response('deleted',Response::HTTP_NO_CONTENT);
    }
}
