<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
  protected $table = 'presensis';
  protected $primaryKey = 'id';
  protected $guarded = [];
  protected $fillable = [
    'tanggal',
    'absen_pagi',
    'absen_sore',
    'id_pegawai'
];
  
   public function pegawai(){
        return $this->belongsTo('App\\Model\\Pegawai','id_pegawai','id');
   }
   public function getRouteKeyName(){
    return 'id_pegawai';
  } 
  
}
