<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawais';
    protected $primaryKey = 'id';
    public function presensi(){
        return $this->hasMany('App\\Model\\Presensi','id_pegawai','id');
    }
    public function getKeyName(){
        return 'id';
    }

    protected $fillable = ['name','alamat','nip','bidang','id_user','status_kepegawaian','jenis_kelamin','role'];

}
