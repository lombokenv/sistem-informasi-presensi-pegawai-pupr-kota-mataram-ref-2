
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import Vuetify from 'vuetify'

// import './plugins/vuetify'
import './stylus/app.styl'

Vue.use(Vuetify,{
    iconfont: 'md',
    theme:{
      primary: '#1c4cb2', //blue-grey darken-4
      navbar2 : '#3367d6',
      deep_purple:'#009688',
      navbar_color:'#283846',
      natural:'#fff',
      grey :'#bcbcbc',
      purple:'#9C27B0',
      red:'#D50000',
      orange:'#FF9800',
      red_darken:'#B71C1C',
      green:'#4CAF50',
      blue_darken:'#1976D2',
    }
  })

import User from './Helpers/User'
window.User = User

window.EventBus = new Vue();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('app-home', require('./components/AppHome.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import router from './Router/router.js'
const app = new Vue({
    el: '#app',
    router
});
