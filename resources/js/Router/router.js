import Vue from 'vue' 
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Login from '../components/login/Login'
import Signup from '../components/login/Signup'
import Cpanel from '../components/paneladmin/Control-panel'
import Dashboard from '../components/paneladmin/Dashboard-menu'
import Add_pegawai from '../components/paneladmin/Add_pegawai'
import View_pegawai from '../components/paneladmin/View_pegawai'
import Presensi_pegawai from '../components/paneladmin/Presensi_pegawai'
import User from '../components/paneladmin/User-menu'
import Logout from '../components/login/Logout'

const routes = [
    { path: '/', component: Login, name:'login'},
    { path: '/login', component: Login },    
    { path: '/logout', component: Logout },    
    { path: '/signup', component: Signup},
    { path: '/cpanel',name:'cpanel',component: Cpanel }, 
    { path: '/dashboard',name:'dashboard',component: Dashboard }, 
    { path: '/add-pegawai',name:'add_pegawai',component: Add_pegawai }, 
    { path: '/view-pegawai',name:'view_pegawai',component: View_pegawai }, 
    { path: '/presensi',name:'presensi',component: Presensi_pegawai }, 
    { path: '/user',name:'user',component: User },
  ]

   

  const router = new VueRouter({
    routes, // short for `routes: routes`
    hashbang : false, //penambahan untuk menghilangkasn # pada url
    mode :'history' //penambahan untuk menghilangkasn # pada url
  })
  
export default router