<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
       $this->call(PegawaiSeed::class);
        $this->call(PresensiSeed::class);
        $this->call(UserSeed::class);
    }
}
