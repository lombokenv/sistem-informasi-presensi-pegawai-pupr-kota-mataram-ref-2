<?php

use Illuminate\Database\Seeder;

class PegawaiSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\Pegawai::class, 5)->create();
    }
}
