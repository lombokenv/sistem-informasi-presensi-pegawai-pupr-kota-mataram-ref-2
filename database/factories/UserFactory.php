<?php

use Faker\Generator as Faker;

$factory->define(App\Model\User::class, function (Faker $faker) {
    static $password;

    return [
        'name'=>"admin",
        'email'=>$faker->unique()->safeEmail,
        'password'=> $password ?: $password = bcrypt('secret'),
        'remember_token'=>str_random(10)
    ];
});
