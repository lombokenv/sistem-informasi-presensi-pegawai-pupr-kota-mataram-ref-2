<?php

use Faker\Generator as Faker;
use App\Model\Pegawai;


$factory->define(App\Model\Presensi::class, function (Faker $faker) {

    return [
        'tanggal'=>$faker->date,
        'absen_pagi'=>$faker->time,
        'absen_sore'=>$faker->time,
        'id_pegawai'=>function(){
           
            return Pegawai::all()->random()->id;
        }
    ];
});
