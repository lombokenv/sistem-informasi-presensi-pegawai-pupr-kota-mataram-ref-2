<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Pegawai::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $status = $faker->randomElement(['PTT', 'PNS']);
    return [
        'name'=>$faker->name,
        'alamat'=>$faker->address,
        'nip'=>$faker->unique()->randomNumber($nbDigits = 8),
        'bidang'=>$faker->word,
        'status_kepegawaian'=>$status,
        'jenis_kelamin'=>$gender
    ];
});
