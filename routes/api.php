<?php

Route::group(['prefix' => '/'], function(){

    Route::apiResource('/pegawai', 'PegawaiController');
    Route::apiResource('/user','UserController');
    Route::get('/status/{any}','PegawaiController@showStatus');
    Route::apiResource('/presensi','PresensiController');
    


});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('signup','AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});


